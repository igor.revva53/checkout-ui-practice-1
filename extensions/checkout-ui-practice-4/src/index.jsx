import React, {useEffect, useState} from 'react';
import {
    render,
    useTranslate,
    useShippingAddress,
    useSettings,
    useBuyerJourneyIntercept
} from '@shopify/checkout-ui-extensions-react';

render('Checkout::Dynamic::Render', () => <App />);

function App() {
    const translate = useTranslate();
    const {address1, address2, zip} = useShippingAddress();
    const {
        errorTextBanner,
        errorTextZip,
        errorTextAddress1,
        errorTextAddress2,
        forbiddenWords,
        forbiddenZipCodes
    } = useSettings();

    function isForbiddenZipCodes(zip, forbiddenZipCodes) {
        if (!forbiddenZipCodes) return false;

        return forbiddenZipCodes.split("\n").includes(zip);
    }

    function isForbiddenWords(string, forbiddenWords) {
        if (!forbiddenWords) return false;

        return forbiddenWords.split("\n").some(forbiddenWord => {
            const lowerCaseForbiddenWord = forbiddenWord.toLowerCase();

            return string.toLowerCase().includes(lowerCaseForbiddenWord);
        });
    }

    useBuyerJourneyIntercept(
        ({canBlockProgress}) => {
            if (canBlockProgress && address1 && isForbiddenWords(address1, forbiddenWords)) {
                return {
                    behavior: "block",
                    reason: "Invalid address1",
                    errors: [
                        {
                            message: errorTextAddress1 ?? translate("error_text_address1"),
                            target: '$.cart.deliveryGroups[0].deliveryAddress.address1'
                        },
                        {
                            message: errorTextBanner ?? translate("error_text_banner")
                        }
                    ]
                }
            }

            if (canBlockProgress && address2 && isForbiddenWords(address2, forbiddenWords)) {
                return {
                    behavior: "block",
                    reason: "Invalid address2",
                    errors: [
                        {
                            message: errorTextAddress2 ?? translate("error_text_address2"),
                            target: '$.cart.deliveryGroups[0].deliveryAddress.address2'
                        },
                        {
                            message: errorTextBanner ?? translate("error_text_banner")
                        }
                    ]
                }
            }

            if (canBlockProgress && zip && isForbiddenZipCodes(zip, forbiddenZipCodes)) {
                return {
                    behavior: "block",
                    reason: "Invalid country zip code",
                    errors: [
                        {
                            message: errorTextZip ?? translate("error_text_zip"),
                            target: '$.cart.deliveryGroups[0].deliveryAddress.postalCode'
                        },
                        {
                            message: errorTextBanner ?? translate("error_text_banner")
                        }
                    ]
                }
            }

            return {
                behavior: "allow"
            }
        }
    );

    return null;
}
