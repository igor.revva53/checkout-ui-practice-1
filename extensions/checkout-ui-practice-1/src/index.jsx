import React, {useEffect, useState} from 'react';
import {
    render,
    Link,
    useSettings,
    useShippingAddress,
    Modal,
    TextBlock
} from '@shopify/checkout-ui-extensions-react';

render('Checkout::Dynamic::Render', () => <App />);

function App() {
    const {link_text, modal_heading, modal_text, states_text} = useSettings();
    const {provinceCode} = useShippingAddress();
    const isValidState = states_text.split("\n").includes(provinceCode);

    if (!isValidState) {
        return null;
    }

    return (
        <Link overlay={
            <Modal padding title={modal_heading}>
                <TextBlock>
                    {modal_text}
                </TextBlock>
            </Modal>
        }>
            {link_text}
        </Link>
    );
}
