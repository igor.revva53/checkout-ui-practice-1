import React, {useEffect, useState} from 'react';
import {
    render,
    Banner,
    useCartLines,
    useSettings,
    useApplyCartLinesChange,
    useTranslate
} from '@shopify/checkout-ui-extensions-react';

render('Checkout::Dynamic::Render', () => <App />);

function App() {
    const {
        variantId,
        productsList,
        enableUpsell,
        bannerTitle,
        bannerText
    } = useSettings();
    const changeCartLines = useApplyCartLinesChange();
    const cartLines = useCartLines();
    const [showBanner, setShowBanner] = useState(false);
    const translate = useTranslate();
    const cartLinesId = cartLines.map(item => item.merchandise.product.id.split("/").pop());
    const isMatchedProductInCart = checkMatchedProductInCart(cartLinesId, productsList);

    function checkMatchedProductInCart(cartLinesId, productsListIdStr) {
        if (!productsListIdStr) return false;

        const productsListIdArr = productsListIdStr.split(",");

        return cartLinesId.some(id => productsListIdArr.includes(id));
    }

    async function addUpsellProduct(variantId) {
        return await changeCartLines({
            type: "addCartLine",
            merchandiseId: variantId,
            quantity: 1
        });
    }

    useEffect(() => {
        if (isMatchedProductInCart && variantId && enableUpsell) {
            addUpsellProduct(variantId).then((res) => {
                if (res.type === "success") {
                    setShowBanner(true);
                } else {
                    console.log(res.message);
                }
            }).catch(e => {
                console.log(e);
            });
        }
    }, [isMatchedProductInCart]);

    useEffect(() => {
        if (!showBanner) return;

        const timer = setTimeout(() => {
            setShowBanner(false);
        }, 5000);

        return () => clearTimeout(timer);
    }, [showBanner]);

    return showBanner ? (
        <Banner title={bannerTitle ?? translate("upsell_product_banner_title")} status="success">
            {bannerText ?? translate("upsell_product_banner_text")}
        </Banner>
    ) : null;
}
