import React from 'react';
import {
    useExtensionApi,
    render,
    Banner,
    useTranslate,
    useApplyCartLinesChange
} from '@shopify/checkout-ui-extensions-react';

render('Checkout::Dynamic::Render', () => <App />);

function App() {
    const {extensionPoint} = useExtensionApi();
    const translate = useTranslate();
    const change = useApplyCartLinesChange();

    console.log(change);

    return (
        <Banner title={extensionPoint}>

            {translate('welcome', {extensionPoint})}
        </Banner>
    );
}
